#!/bin/python3
import csv, random, argparse
import tensorflow as tf
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
import csv, random, argparse
from string import punctuation
import numpy as np
from numpy import array
from numpy import asarray
#from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import time
import datetime
import pickle
from sklearn import svm
import re
embedding_dim = 300
filter_sizes = [3,4,5]
#num_filters = 128
l2_reg_lambda=  1
dropout_keep_prob =0.7
batch_size = 1
num_epochs = 20
evaluate_every =500
#embed_size = 100
#np.random.seed(14353)

def normalizing(x, axis):    
    norm = tf.sqrt(tf.reduce_sum(tf.square(x), axis=axis, keep_dims=True))
    normalized = x / (norm)   
    return normalized

def batch_iter(data, batch_size, num_epochs, shuffle=True):
    """
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int((len(data)-1)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]

class TextCNN(object):
    
    def __init__(
      self, sequence_length, num_classes, vocab_size,
      embedding_size, filter_sizes, num_filters, l2_reg_lambda=0.0):

        # Placeholders for input, output and dropout
        self.input_x = tf.placeholder(tf.int32, [None, sequence_length], name="input_x")
        self.input_y = tf.placeholder(tf.int32, [None], name="input_y")
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        labels = tf.one_hot(self.input_y, num_classes)

        # Keeping track of l2 regularization loss (optional)
        l2_loss = tf.constant(0.0)

        # Embedding layer
        with tf.device('/GPU:0'), tf.name_scope("embedding"):
            self.W = tf.Variable(
                tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0),
                name="W")
            self.embedded_chars = tf.nn.embedding_lookup(self.W, self.input_x)
            self.embedded_chars_expanded = tf.expand_dims(self.embedded_chars, -1)

        # Create a convolution + maxpool layer for each filter size
        pooled_outputs = []
        for i, filter_size in enumerate(filter_sizes):
            with tf.name_scope("conv-maxpool-%s" % filter_size):
                # Convolution Layer
                filter_shape = [filter_size, embedding_size, 1, num_filters]
                W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
                b = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
                conv = tf.nn.conv2d(
                    self.embedded_chars_expanded,
                    W,
                    strides=[1, 1, 1, 1],
                    padding="VALID",
                    name="conv")
                # Apply nonlinearity
                #h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                h = tf.nn.bias_add(conv, b)
                # Maxpooling over the outputs
                pooled = tf.nn.max_pool(
                    h,
                    ksize=[1, sequence_length - filter_size + 1, 1, 1],
                    strides=[1, 1, 1, 1],
                    padding='VALID',
                    name="pool")
                pooled_outputs.append(pooled)

        # Combine all the pooled features
        num_filters_total = num_filters * len(filter_sizes)
        self.h_pool = tf.concat(pooled_outputs, 3)
        self.h_pool_flat = tf.reshape(self.h_pool, [-1, num_filters_total])

        # Add dropout
        with tf.name_scope("dropout"):
            self.h_drop = tf.nn.dropout(self.h_pool_flat, self.dropout_keep_prob)

        # Final (unnormalized) scores and predictions
        with tf.name_scope("output"):
            W = tf.get_variable(
                "W",
                shape=[num_filters_total, num_classes],
                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")
            l2_loss += tf.nn.l2_loss(W)
            l2_loss += tf.nn.l2_loss(b)
            self.scores = tf.nn.xw_plus_b(self.h_drop, W, b, name="scores")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")

        # Calculate mean cross-entropy loss
        with tf.name_scope("loss"):
            losses = tf.nn.softmax_cross_entropy_with_logits(logits=self.scores, labels=labels)
            self.loss = tf.reduce_mean(losses) + l2_reg_lambda * l2_loss

        # Accuracy
        with tf.name_scope("accuracy"):
            correct_predictions = tf.equal(self.predictions, tf.argmax(labels, 1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")

def train_model(x_train, y_train, vocab_len, x_dev, y_dev, num_filters, x_test, y_test):
    
    with tf.Graph().as_default():
        sess = tf.Session()
        with sess.as_default():
            cnn = TextCNN(
                sequence_length=x_train.shape[1],
                num_classes=5,
                vocab_size = vocab_len,
                embedding_size = embedding_dim,
                filter_sizes= filter_sizes,
                num_filters= num_filters,
                l2_reg_lambda=l2_reg_lambda)

            # Define Training procedure
            global_step = tf.Variable(0, name="global_step", trainable=False)
            optimizer = tf.train.GradientDescentOptimizer(1e-3)
            grads_and_vars = optimizer.compute_gradients(cnn.loss)
            train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

            #saver = tf.train.Saver(tf.global_variables(), max_to_keep=FLAGS.num_checkpoints)
            saver = tf.train.Saver()
            # Initialize all variables
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())

            def train_step(x_batch, y_batch):
                feed_dict = {
                  cnn.input_x: x_batch,
                  cnn.input_y: y_batch,
                  cnn.dropout_keep_prob: dropout_keep_prob
                }
                _, step,loss, accuracy = sess.run(
                    [train_op, global_step, cnn.loss, cnn.accuracy],
                    feed_dict)
                time_str = datetime.datetime.now().isoformat()
                print("{}: step {},loss {:g}, acc {:g}".format(time_str, step,loss, accuracy))
                return loss
                

            def dev_step(x_dev, y_dev):
                dev_batches = batch_iter(list(zip(x_dev, y_dev)), batch_size, 1)
                avg_loss=0
                avg_acc = 0
                count = 0
                for batch in dev_batches:
                    x_batch, y_batch = zip(*batch)
                    feed_dict = { cnn.input_x: x_batch,cnn.input_y: y_batch,cnn.dropout_keep_prob: 1.0}
                    step,  loss, accuracy = sess.run( [global_step,  cnn.loss, cnn.accuracy],feed_dict)
                    avg_loss+=loss
                    avg_acc+=accuracy
                    count+=1

                avg_loss=avg_loss/float(count)
                avg_acc/=float(count)
                time_str = datetime.datetime.now().isoformat()
                print("{}: step {}, loss {:g}, acc {:g}".format(time_str, step, avg_loss, avg_acc))
                return avg_acc
                
            # Generate batches
            batches = batch_iter(
                list(zip(x_train, y_train)), batch_size, num_epochs)
            # Training loop. For each batch...
            for batch in batches:
                x_batch, y_batch = zip(*batch)
                train_loss = train_step(x_batch, y_batch)
                current_step = tf.train.global_step(sess, global_step)
                if current_step % evaluate_every == 0:
                    print("\nEvaluation:")
                    a = dev_step(x_dev, y_dev)
                    print("")
            
            print('Final Test Accuracy:')
            test_acc = dev_step(x_test, y_test)
            feed_dict = {
                  cnn.input_x: x_train,
                  cnn.input_y: y_train,
                  cnn.dropout_keep_prob: 1.0
                }
            final_train_loss = sess.run([cnn.loss], feed_dict)

            save_path = saver.save(sess, "sst-imdb/model.ckpt")
            print("Model saved in path: %s" % save_path)

            print("Last:", final_train_loss, test_acc)
            return final_train_loss,test_acc

class SNLIClassifier:
    def __init__(self):
        train_file = open('data/train.csv', 'r')
        self.train_reader = csv.reader(train_file)

        valid_file = open('data/valid.csv', 'r')
        self.valid_reader = csv.reader(valid_file)

        test_in_file = open('data/test.csv', 'r')
        self.test_reader = csv.reader(test_in_file)

        # No header row to skip
    

    def train(self, subsample_ratio, filters):
        
        sentences = list()
        labels = list()
        for i,entry in enumerate(self.train_reader):
            sen = entry[0]
            sen = re.sub(r"n\'t", " not", sen) 
            sen = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", sen)   
            sen = re.sub(r"\s{2,}", " ", sen)
            sentences.append(sen.strip().lower())
            labels.append(entry[1])
        
        combined = list(zip(sentences, labels))
        random.shuffle(combined)
        sentences, labels = zip(*combined)
        #take sample ratios
        if subsample_ratio > len(sentences):
            subsample_ratio = len(sentences)
        sentences = sentences[:int(len(sentences)*subsample_ratio)]
        labels = labels[:int(len(labels)*subsample_ratio)]

        self.tokenizer = Tokenizer(oov_token='UNK')
        self.tokenizer.fit_on_texts(sentences)
        encoded_docs = self.tokenizer.texts_to_sequences(sentences)
        X_train = pad_sequences(encoded_docs, maxlen= 49, padding='post')
        Y_train = np.array(labels)
        vocab_size = len(self.tokenizer.word_index) + 1

        sentences = list()
        labels = list()
        for i,entry in enumerate(self.valid_reader):
            sen = entry[0]
            sen = re.sub(r"n\'t", " not", sen) 
            sen = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", sen)   
            sen = re.sub(r"\s{2,}", " ", sen)
            sentences.append(sen.strip().lower())
            labels.append(entry[1])

        self.tokenizer.fit_on_texts(sentences)
        encoded_docs = self.tokenizer.texts_to_sequences(sentences)
        X_dev = pad_sequences(encoded_docs,maxlen= 49, padding='post')
        Y_dev = np.array(labels)

        sentences = list()
        labels = list()
        for i,entry in enumerate(self.test_reader):
            sen = entry[0]
            sen = re.sub(r"n\'t", " not", sen) 
            sen = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", sen)   
            sen = re.sub(r"\s{2,}", " ", sen)
            sentences.append(sen.strip().lower())
            labels.append(entry[1])

        self.tokenizer.fit_on_texts(sentences)
        encoded_docs = self.tokenizer.texts_to_sequences(sentences)
        X_test = pad_sequences(encoded_docs,maxlen= 49, padding='post')
        Y_test = np.array(labels)

            
        train_loss, test_acc = train_model(X_train, Y_train, vocab_size, X_dev, Y_dev, filters, X_test,Y_test)
          

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--subsample_ratio', nargs='?', const=1.0, type=int)
    parser.add_argument('--filters', nargs='?', const=100, type=int)

    args = parser.parse_args()
    trainer = SNLIClassifier()

    print('Training...')
    trainer.train(args.subsample_ratio, args.filters)

